$(document).ready(function() {

    $('h3#success').click(function() {
        $('p.message_success').fadeToggle();
    })

    $('h3#error').click(function() {
        $('p.message_error').slideToggle();
    })
});
